classdef RotaryEncoder < handle
    
    properties
        arduino
    end
    
    methods
        
        function self = RotaryEncoder(port)
            self.connect(port);
        end
        
        function connect(self, port)
            % Check whether ACM port is recognized under 'port'
            if isunix
                
                % Get info on available ports
                avaSP = instrhwinfo('serial');
                fprintf(1, 'Checking availability of ACM port under %s ... \n', port)
                
                % Check whether symlink port is available AND create if the
                % symlink if necessary.
                while ~any(strcmp(avaSP.AvailableSerialPorts, port))
                    fprintf(1, 'Matlab currently fails to recognize Arduino''s port.\nTo change that and create a symlink pls enter the pw below:\n');
                    [status, result] = system(['sudo ln -s /dev/ttyACM0 ', port]);
                    
                    % Check whether the installation of the symlink was
                    % successfull.
                    if status 
                        fprintf(1, '\nTrying to establish the symlink failed.\n');
                        fprintf(1, 'System message: %s\n', result); 
                        return;
                    end
                        
%                         % Communicate option to remove and reinstall the
%                         % symlink
%                         fprintf(1, 'I can remove the symlink and retry the installation.\nIf you have tried this already, and it failed, there is a deeper issue and I recommend aborting.\n')
%                         o = input('Do you want me to remove the old symlink and try to re-installit [y / n]? ','s');
%                         if strcmp(o,'y')
%                             [status, result] = system(['sudo rm ', port]);
%                         else
%                             fprintf(1, 'You have decided to abort the reinstallation. Abort mission.')
%                             WaitSecs(.2); return;
%                         end
%                     end
                    avaSP = instrhwinfo('serial');
                    if any(strcmp(avaSP.AvailableSerialPorts, port))
                        fprintf(1, '\nPort available.\n');
                    end
                end
            end
            
            % Configure serial port
            self.arduino = serial(port, 'BaudRate', 115200);
            fprintf(1,['Trying to connect to Arduino at "', port, '"...\n']);
            try
                fopen(self.arduino);
            catch ME
                disp('Connection could not be established.');
                rethrow(ME);
            end
            % Wait for serial interface of Arduino to be ready
            WaitSecs(1);
            fprintf(1,'Connection established.\n');
        end
        
        function disconnect(self)
            fclose(self.arduino);
        end
        
        function delete(self)
            self.disconnect();
        end
        
        function requestStatus(self)
            fprintf(self.arduino, 1);
        end
        
        function [encoder, button] = receiveStatus(self)
            data = fscanf(self.arduino, '%d %d');
            encoder = data(1);
            button= data(2);
        end
        
        function [encoder, button] = getStatus(self)
            self.requestStatus();
            [encoder, button] = self.receiveStatus();
        end
        
        function resetEncoder(self)
            fprintf(self.arduino, 2);
        end
        
    end
end

