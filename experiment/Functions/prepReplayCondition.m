function [ms, start, abort] = prepReplayCondition(vpno, seno, coow, start)

% reads in the various data for the ms conditions and makes sure that
% everything is as it should be
% input: vp number, session number
% output: array with ms coords, velocities, onsets, and fitted main
% sequence parameters
%
% 06/2020 by Jan Klanke

% stopping vat
abort = 0;

% function to easily filter the names of the files for a certain pattern.
sf = @(cll,ptn) ~cellfun('isempty', regexp(cll,ptn));

% get last session no of completed sessions
if coow; sesnum = 0;
else sesnum = str2double(seno) - 1; end

% predefine ms structures
ms  = struct('coords',[],'onsets',[],'params',[]);
old = struct('coords',[],'onsets',[],'params',[]);
 
% filter files of participant
filesAll = dir(sprintf('Data/replayCoords/%s*processed.csv',vpno));

% switch for session number
switch sesnum
    case 0
        fprintf(1,'\n>>>> Saccade data status: no saccade data');
        fprintf(1,'\n>>>> ...proceed...\n');
        WaitSecs(0.5); 
        start = 0;
        
    otherwise
        % search for files and select relevant subset        
        prev_seno = sprintf( '%02d', sesnum);
        sessionIdx = sf({filesAll.name}, strcat(vpno,prev_seno));         % index of sessions of this vp
        files = filesAll(sessionIdx);                                     % files cleaned by session
        
        % communicate successful find
        if isempty(files)
            fprintf(1,'\n>>>> Saccade data status: NOT available...'); 
            fprintf(1,'\n>>>> ...abort...\n'); 
            abort = 1; return;
        else
            fprintf(1,'\n>>>> Saccade data status: available...'); 
        end
        
        % load in ms data of all available files
        ms.coords = importdata(files(sf({files.name}, 'msCoords')).name, ',');
        ms.onsets = importdata(files(sf({files.name}, 'onsetTimes')).name, ',');
        ms.params = importdata(files(sf({files.name}, 'mainSequence')).name, ',');

        if sesnum > 1
            sessionIdx_old = sf({filesAll.name}, sprintf( '%s%02d', vpno, sesnum-1));   % index of PREVIOUS sessions of this vp
            files_old = filesAll(sessionIdx_old);                                        % files of PREVIOUS session

            old.coords = importdata(files_old(sf({files_old.name}, 'msCoords')).name, ',');
            old.onsets = importdata(files_old(sf({files_old.name}, 'onsetTimes')).name, ',');
        end
        
         % communicate successful load
        while ~any(structfun(@isempty, ms))
                % check whether data is bigger than in previous sessions (if
                % necessary)
                if sesnum > 1 && ~(size(old.coords.data, 1) < size(ms.coords.data, 1) && size(old.onsets.data, 1) < size(ms.onsets.data, 1))
                    fprintf(1,'\n>>>> I was not able to load these files...');
                    fprintf(1,'\n>>>> ...abort...\n');
                    abort = 1; return;
                end
                
                % show files
                fprintf(1,'\n>>>> I was able to load these files...');
                fprintf(1,'\n>>>> (*) %s', (files.name));
                o = input('\n\n>>>> Do you want to continue [y / n]? ','s');
                
                % allow experimenter to decide whether they want to
                % continue
                if strcmp(o,'y')
                    fprintf(1,'>>>> ...proceed...\n');
                    WaitSecs(0.5); 
                    start = 0; break;
                else
                    ms = struct('coords',[],'onsets',[],'params',[]);
                end
        end
        if any(structfun(@isempty, ms))
            fprintf(1,'\n>>>> I was not able to load these files...');
            fprintf(1,'\n>>>> ...abort...\n');
            abort = 1; return;
        end
end

end

