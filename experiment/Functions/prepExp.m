function [vpno, seno, cond, coow, dome, subjectCode] = prepExp(experimentName)
%
% asks for subject-ID
% input: exptstr (string) name of experiment
% output: participant id (vpno; string), session no. (seno; string),
% condition indicies(cond; numeric), switch overwriting the default 
% conditions for sessions with that of 1st one (coow,logical), dominant eye
% (dome, string), and subject code for filename (subjectCode, string)
%
% 2020 by Jan Klanke 

FlushEvents('keyDown');
global setting

%%%%%%%%%%%%%%%%%%%%%%
% Get participant ID %
%%%%%%%%%%%%%%%%%%%%%%
start = 1;
while start
    vpno = input('>>>> Enter participant ID:  ','s');
    if isempty(vpno); vpno = sprintf('x');
    end
    if length(vpno) == 1; start = 0;
    elseif length(vpno) > 1; fprintf(1, 'WARNING:  Subject code too long. Pls only use 1 letter/number.\n')
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Default all values (except vpno) for training %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if setting.train
    seno = '01'; cond = 2; coow = 0;
    subjectCode = sprintf(strcat(experimentName,'p',vpno,seno));
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get session number (no train) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
    seno = input('>>>> Enter session number:  ','s');
    if length(seno)==1; seno = strcat('0',seno);
    elseif isempty(seno); seno = sprintf('01'); 
    end
    
    % create subject code based on experiment name, vopno, and seno.
    subjectCode = sprintf(strcat(experimentName,'_',vpno,seno));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Get conditions (no train)   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    cond = NaN;
    coow = 0;

    if str2double(seno) == 1; defaultConds = [1 2 3];
    else defaultConds = [1 4 5]; end

    % ask for conditions
    while length(intersect(cond, defaultConds)) < length(cond)
        cond = input('>>>> Enter condition numbers:  ','s');
        if length(cond) >= 1; cond = str2num(cond);
        elseif isempty(cond); cond = defaultConds;
        end

        if length(intersect(cond, defaultConds)) < length(cond)
            fprintf(1, 'WARNING:  At least one of the conditions you specified does not exist. \nWARNING:  You can normally only choose between conditions: %s\n', num2str(defaultConds));
            fprintf(1, 'WARNING:  However, I can overwrite the defaults for this session.\n')
            overwrite = input( 'WARNING:  Do you want me to override the session defaults [y / n]? ','s');
            if strcmp(overwrite,'y')
                 coow = 1;break;
            end
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%
%   Get dominant eye   %
%%%%%%%%%%%%%%%%%%%%%%%%
defaultEye = ['L', 'R', 'B'];
dome = NaN;

% ask for dominant eye 
while length(intersect(dome, defaultEye)) < length(dome)
    dome = input('>>>> Enter dominant eye [e.g. L, R, B]:  ','s');
    if isempty(dome); dome = 'R';
    end
    if length(intersect(dome, defaultEye)) < length(dome); fprintf(1, 'WARNING:  You made a mistake when trying to specify the dominant eye. \nWARNING:  You can choose between the following options: L, R, B\n');
    end
end