function prepScreen
%
% 2016 by Martin Rolfs || adapted 2019 by Jan Klanke

global scr setting

% If there are multiple displays guess that one without the menu bar is the
% best choice.  Dislay 0 has the menu bar.
scr.allScreens = Screen('Screens');
scr.expScreen  = max(scr.allScreens);

% Initialize for unified KbName's and normalized 0 - 1 color range:
PsychDefaultSetup(2);

% get colours
scr.bgLum = 0.5;
scr.black = BlackIndex(scr.expScreen);
scr.white = WhiteIndex(scr.expScreen);
scr.gray = GrayIndex(scr.expScreen);
scr.bgColor = scr.black + (scr.bgLum * (scr.white - scr.black)); 
scr.fgColor = scr.white; 
scr.fNyquist = 0.5;

if setting.Pixx == 1
    prepScreenPixx
else
    scr.subDist = 300;          % subject distance (mm)
    scr.width   = 285.0;        % width  of screen (mm) 
    scr.height  = 180.0;        % height of screen(mm)en (cm)
    scr.rate    = 1;            % number of stimulus updates per refresh
    scr.refr    = 60; % screen refresh rate (Hz)
    
    % Open the normal screen
    [scr.main,scr.rect] = PsychImaging('OpenWindow', scr.expScreen, scr.bgColor);
    scr.myimg = scr.main; % for easy pickings; equate pixx buffer handle with screen image handle 
end

% change font size
Screen('TextFont', scr.myimg,'-adobe-helvetica-medium-o-normal--25-180-100-100-p-130-iso8859-1');

% determine th main window's center
[scr.centerX, scr.centerY] = WindowCenter(scr.myimg);
[scr.resX   , scr.resY   ] = WindowSize(scr.myimg);
scr.rect = [0 0 scr.resX scr.resY];

% calculate refresh rate, frame duration and pixel-per-degree
scr.refr = scr.rate * Screen('NominalFrameRate',scr.main); % screen refresh rate (Hz)
scr.fd   = 1/scr.refr;      % frame duration (sec)
scr.ppd  = dva2pix(1,scr);  % pixel per degree

% communicate fd.
fprintf(1,'\n\nScreen runs at %.1f Hz.\n\n',1/scr.fd);

% Give the display a moment to recover from the change of display mode when
% opening a window. It takes some monitors and LCD scan converters a few seconds to resync.
WaitSecs(2);
% hide cursor if not in dummy mode
ShowCursor('CrossHair');