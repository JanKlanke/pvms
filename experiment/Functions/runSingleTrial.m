function [data,eyeData] = runSingleTrial(td)
%
% td = trial td
%
% 2016 by Martin Rolfs 
% adapted 2019 by Jan Klanke

global scr visual setting re keys

% clear keyboard buffer
FlushEvents('KeyDown');
if ~setting.TEST == 1, HideCursor(); end
pixx = setting.Pixx;

% Set the transparency for gabor patch
% Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);

% function for rounding away from zero
ceilfix = @(x)ceil(abs(x)) .* sign(x);

% predefine boundary information
cxm = td.fixa.loc(1);
cym = td.fixa.loc(2);
rad = visual.boundRad;
chk = visual.fixCkRad;

% draw trial information on operator screen
if ~setting.TEST, Eyelink('command','draw_box %d %d %d %d 15', (cxm-chk)*2, (cym-chk)*2, (cxm+chk)*2, (cym+chk)*2); end

% generate Procedural Gabor textures
nStim = length(td.stims.pars.sizp);
sti.tex = visual.procGaborTex;
sti.vis = td.stims.pars.sizp;
sti.src = [zeros(2,nStim); sti.vis; sti.vis]';
sti.dst = CenterRectOnPoint(sti.src, td.stims.locX, td.stims.locY);
for f = 1:td.totNFr; stiFrames(f).dst = sti.dst + repmat(td.posVec(:,f),2,nStim)'; end

% predefine time stamps
tFixaOn = NaN;  % t of fixation on
tFixaOf = NaN;  % t of fixation on
tStimOn = NaN;  % t of stimulus stream on
tStimCf = NaN;  % t of stimulus at full contrast
tStimMS = NaN;  % t of stimulus staring to move
tStimMT = NaN;  % t of stimulus staring to move
tStimCd = NaN;  % t of stimulus no longer at full contrast
tStimOf = NaN;  % t of stimulus off
tRes    = NaN;  % t of response (if any)
tClr    = NaN;  % t of of clear screen

% set flags before starting stimulus stream
eyePhase  = 1;  % 1 is fixation phase, 2 is saccade phase
breakIt   = 0;
fixBreak  = 0;
saccade   = 0;

% eyeData
eyeData = [];

% Initialize vector important for response
pressed = 0; spaKey = 0;

% Initialize vector to store data on timing
frameTimes = NaN(1,td.totNFr);

% flip screen to start out time counter for stimulus frames
firstFlip = 0;nextFlip = 0;
while ~firstFlip
    if pixx, firstFlip = PsychProPixx('QueueImage', scr.myimg);
    else firstFlip = Screen('Flip', scr.myimg); end
end

f = 0;              % set frame count to 0
tLoopBeg = GetSecs; % get a first timestap
t = tLoopBeg;

while ~breakIt && f < td.totNFr
    f = f+1;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % stimulus presentation %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
    for slo = 1:setting.sloMo
        Screen('FillRect', scr.myimg, visual.bgColor);

        % stimuli
        % Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);
        if td.stims.vis(f)
            Screen('DrawTextures', scr.myimg, sti.tex, sti.src, stiFrames(f).dst, td.stims.pars.ori, [], [], [], [], kPsychDontDoRotation, [td.stims.pha(:,f), td.stims.pars.frqp', td.stims.pars.sigp', td.stims.amp(:,f), td.stims.pars.asp', zeros(nStim,3)]');
            drawCirc(visual.bgColor, td.fixa.loc, visual.fixCkRad, scr.myimg);
        end
        % clockface
        if td.clock.vis(f)  
            Screen('DrawDots', scr.myimg, td.clock.facePos, 3, td.clock.faceCol, [], 1); % draws clockface by means of 3 pixel dots
            drawClockhandL(td.onPos+f-1, td.clock.handPos, td.clock.handWidth, td.clock.handCol, td.clock.nHand, scr.myimg);
        end
        % fixation
        if td.fixa.vis(f)
            drawFixation(td.fixa.col, td.fixa.loc, td.fixa.sin, scr.myimg);            
        end
        % target
        if td.targ.vis(f)
            drawFixation(td.targ.col, td.targ.loc, td.targ.sin, scr.myimg);            
        end
        % Flip
        if pixx, nextFlip = PsychProPixx('QueueImage', scr.myimg);
        else nextFlip = Screen('Flip', scr.myimg); end 
        % screen2gif(f,sprintf('Data/gif/%s%02d%02d%02d_%04d.png',td.vpno,td.seno,td.block,td.trial,f)); 
    end
    frameTimes(f) = GetSecs;
    
    %%%%%%%%%%%%%%%%%%%%%%%%
    % raise stimulus flags %
    %%%%%%%%%%%%%%%%%%%%%%%%

    % Send message that stimulus is now on
    if isnan(tFixaOn) && td.events(f)==1
        if ~setting.TEST  ; Eyelink('message', 'EVENT_FixaOn'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_FixaOn'); end
        tFixaOn = frameTimes(f);
    end
    if isnan(tFixaOf) && td.events(f)==2
        if ~setting.TEST  ; Eyelink('message', 'EVENT_FixaOf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_FixaOf'); end
        tFixaOf = frameTimes(f);
    end
    if isnan(tStimOn) && td.events(f)==3
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOn'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOn'); end
        tStimOn = frameTimes(f);
    end
    if isnan(tStimCf) && td.events(f)==4
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCf'); end
        tStimCf = frameTimes(f);
    end
    if isnan(tStimMS) && td.events(f)==5
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimMS'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimMS'); end
        tStimMS = frameTimes(f);
    end
    if isnan(tStimMT) && td.events(f)==6
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimMT'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimMT'); end
        tStimMT = frameTimes(f);
    end
    if isnan(tStimCd) && td.events(f)==7
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCd'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCd'); end
        tStimCd = frameTimes(f);
    end
    if isnan(tStimOf) && td.events(f)==8
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOf'); end
        tStimOf = frameTimes(f);
    end
    
    % eye position check
    if setting.TEST<2
        [x,y] = getCoord;
        if sqrt((x-cxm)^2+(y-cym)^2)>chk    % check fixation in a circular area
            fixBreak = 1;
        end
    end
    if fixBreak
        breakIt = 1;    % fixation break
    end
    t = GetSecs;
end

lastFlip = 0;
while ~lastFlip && ~nextFlip
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, lastFlip = PsychProPixx('QueueImage', scr.myimg);
    else lastFlip = Screen('Flip', scr.myimg); end 
end
tLoopEnd = GetSecs;

%%%%%%%%%%%%%%%%%%%%%%%%%
% stimulus presentation %
%%%%%%%%%%%%%%%%%%%%%%%%%
% Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
for i = 1:scr.rate
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, PsychProPixx('QueueImage', scr.myimg);
    else Screen('Flip', scr.myimg); end
end

% Init rotary encoder, reset if necessary
if pixx
    ticks_raw = re.getStatus();
    if ticks_raw ~= 0, re.resetEncoder(); end
end
WaitSecs(td.aftKey/4);

switch breakIt
    case 1
        data = 'fixBreak';
        if setting.TEST<2, Eyelink('command','draw_text 100 100 15 Fixation break'); end
    otherwise
        % Snd('Play',[repmat(0.5,1,1050) linspace(0.5,0.0,50)].*[zeros(1,1000) sin(1:100)],5000);
        % nec. Parameters
        
        tResBeg = GetSecs;
        while ~pressed
            
            % get rotary encoder and response tick(s)
            if pixx; [ticks_raw, pressed] = re.getStatus(); else ticks_raw = 0; end
            ticks_raw = -ticks_raw + td.onPosR;         % add onset position of clock hand and flip ticks
            ticks_raw(ticks_raw < 0) = -ceilfix(ticks_raw / td.respOpt) * td.respOpt + ticks_raw(ticks_raw < 0);  % make sure that ticks are always between 0 and max repsonse options
           
            % create response graphics for clock 
            for i = 1:scr.rate
                Screen('FillRect', scr.myimg, visual.bgColor);         % draw background
                Screen('DrawDots', scr.myimg, td.clock.facePos, 3, td.clock.faceCol, [], 1); % draws clockface by means of 3 pixel dots
                drawClockhandL(ticks_raw, td.clock.handPos, td.clock.handWidth, td.clock.handCol, td.clock.nHand, scr.myimg); % draw reporthand
                if pixx, PsychProPixx('QueueImage', scr.myimg);
                else Screen('Flip', scr.myimg); end 
            end
            
            % end loop and get response time
            if checkTarPress(keys.resSpace)
                pressed = 1; spaKey = 1;
            end
            tRes = GetSecs();                
            
        end
        
        % reset encoder 
        if pixx, re.resetEncoder(); end
        
        % evaluate responses
        if spaKey == pressed
            ticks_raw = NaN;                               % see below for explanations
            ticks_cor = NaN; 
            ticks_corOn = NaN;
            repDeg_raw = NaN;                            
            repDeg_cor = NaN;
            precMs = (tRes - tLoopBeg) * 1000 - td.appbeg;
            tResDur     = (tRes - tLoopBeg) * 1000;      % differences in timestamps between the beginn of presentation and pressing of the space bar [ms]
        elseif spaKey ~= pressed
            if ticks_raw < td.onPos                         % account for the fact that there are 2 clock hands (and you don"t know which is the one
                ticks_cor = ticks_raw + td.respOpt/2;       % reported on by the participant i.e. add respOption/2 if... 
            elseif ticks_raw > td.ofPos                     % a) response position is before onset position and...
                ticks_cor = ticks_raw - td.respOpt/2;       % b) subtract the same if it is after its offset position.
            else
                ticks_cor = ticks_raw;
            end
            ticks_corOn = ticks_cor - td.onPos;                % finally, recalculate the position of the clockhand relative to trialonset
            repDeg_raw  = (ticks_raw / td.respOpt) * 360;      % calculate onset position in degree for UNCORRECTED report index [deg]
            repDeg_cor  = (ticks_cor / td.respOpt) * 360;                   % calculate onset position in degree for CORRECTED report index[deg]
            precMs  = (ticks_cor - td.appbeg * scr.fd) * (scr.fd*1000);     % precision of response [ms]  
            tResDur = (tRes - tResBeg) * 1000;                              % difference in timestamps between the beginning of the response period and the pressing of the power mate [ms]
        end
        WaitSecs(td.aftKey);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%
        % feedback presentation %
        %%%%%%%%%%%%%%%%%%%%%%%%%
        if setting.train
            for f = 1:td.febNFr
                for slo = 1:setting.sloMo
                    if isnan(ticks_raw)
                        Screen('FillRect', scr.myimg, visual.bgColor);           % Paint background
                        
                        % let participants know that you want them to
                        % respond /w the rotation knob
                        str = sprintf('Please use the rotation knob!');
                        drawText(str, td.fixa.loc(1), td.fixa.loc(2));           % draw feedback text  
                        if pixx, nextFlip = PsychProPixx('QueueImage', scr.myimg);
                        else nextFlip = Screen('Flip', scr.myimg); end
                    
                    elseif ~isnan(ticks_raw)
                        Screen('FillRect', scr.myimg, visual.bgColor);           % Paint background

                        % draw clock
                        Screen('DrawDots', scr.myimg, td.clock.facePos, 3, td.clock.faceCol, [], 1); % draws clockface by means of 3 pixel dots
                        
                        % draw actual response
                        drawClockhandL(ticks_raw, td.clock.handPos, td.clock.handWidth, td.clock.handCol, td.clock.nHand, scr.myimg);  % draw clockhand of participant response
                        
                        % draw correct response
                        drawClockhandL(td.fbPos, td.clock.handPos, td.clock.handWidth, visual.white, td.clock.nHand, scr.myimg); % draw clockhand of correct response
                        if pixx, nextFlip = PsychProPixx('QueueImage', scr.myimg);
                        else nextFlip = Screen('Flip', scr.myimg); end 
                    end
                end
            end
        end
        
        lastFlip = 0;
        while ~lastFlip && ~nextFlip
            Screen('FillRect', scr.myimg, visual.bgColor);
            if pixx, lastFlip = PsychProPixx('QueueImage', scr.myimg);
            else lastFlip = Screen('Flip', scr.myimg); end 
        end
        
        for f = 1:scr.rate
            Screen('FillRect', scr.myimg, visual.bgColor);
            if pixx, PsychProPixx('QueueImage', scr.myimg);
            else Screen('Flip', scr.myimg); end 
        end
        
        tClr = GetSecs;
        if setting.TEST<2, Eyelink('message', 'EVENT_Clr'); end
        if setting.TEST; fprintf(1,'\nEVENT_Clr'); end 
        
        %-------------------------%
        % PREPARE DATA FOR OUTPUT %
        %-------------------------%
        % collect trial information
        condData = sprintf('%i\t%i\t%i',...
            [td.expcon td.feedback setting.train]);                         % in results, cells  6:8
        
        % collect stimulus information                                        in results, cells 9:19
        stimData = sprintf('%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%i\t%i\t%.2f\t%i\t%i\t%.2f',...  
            [td.fixpox td.fixpoy td.appbeg td.apptop td.appdur td.appmdX td.appmdY_scr td.stiori_scr td.appori_scr td.appmdY_sac td.stiori_sac td.appori_sac td.appamp ]);
        
        % collect data on the artificial saccade that shaped the stim.        in results, cells 20:24
        asacData = sprintf('%i\t%.2f\t%.2f\t%.2f\t%.2f',...
            [td.sac.req td.sac.amp td.sac.vpk td.sac.dur td.sac.ang]);
        
        % collect clock information                                           in results, cells 25:29
        clockData = sprintf('%i\t%.2f\t%i\t%i\t%i',...
            [td.cSpeed td.cSpeedpDgr td.respOpt td.onPos td.fbPos]); 
                           
        % collect time information                                            in results, cells 30:39
        timeData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i',...                             
            round(1000*([tFixaOn tFixaOf tStimOn tStimCf tStimMS tStimMT tStimCd tStimOf tRes tClr]-tStimOn)));
        
        % collect response information                                        in results, cells 40:48
        respData = sprintf('%i\t%i\t%i\t%i\t%.2f\t%.2f\t%.2f\t%.2f',...                              
            [spaKey ticks_raw ticks_cor ticks_corOn repDeg_raw repDeg_cor precMs tResDur]);               

        % get information about how timing of frames worked                   in results, cells 49:50
        tLoopFrames = round((tLoopEnd-tLoopBeg)/scr.fd);        
        frameData = sprintf('%i\t%i',tLoopFrames,td.totNFr);

        % collect data for tab [3 x condData, 10 x trialData, 5 x sacData, 10 x timeData %i, 8 x respData, 2 x frameData]
        data = sprintf('%s\t%s\t%s\t%s\t%s\t%s\t%s',...
            condData, stimData, asacData, clockData, timeData, respData, frameData);
end
