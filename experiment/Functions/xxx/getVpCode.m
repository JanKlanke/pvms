function [vpno, seno, cond, dome, owsw, subjectCode] = getVpCode(experimentName)
%
% asks for subject-ID
% input: exptstr (string) name of experiment
%
% 2019 by Jan Klanke
% 2020 by Jan Klanke (major revision)

FlushEvents('keyDown');
global setting

flag_1 = 1; 
owsw = 0;

%%%%%%%%%%%%%%%%%%%%%%
% Get participant ID %
%%%%%%%%%%%%%%%%%%%%%%
while flag_1
    vpno = input('>>>> Enter participant ID:  ','s');
    if isempty(vpno); vpno = sprintf('x');
    end
    if length(vpno) == 1; flag_1 = 0;
    elseif length(vpno) > 1; fprintf(1, 'WARNING:  Subject code too long. Pls only use 1 letter/number.\n')
    end
end

%%%%%%%%%%%%%%%%%%%%%%
% Get session number %
%%%%%%%%%%%%%%%%%%%%%%
seno = input('>>>> Enter session number:  ','s');
if length(seno)==1; seno = strcat('0',seno);
elseif isempty(seno); seno = sprintf('01');
end

%%%%%%%%%%%%%%%%%%%%%%
%   Get conditions   %
%%%%%%%%%%%%%%%%%%%%%%
cond = NaN;
if str2double(seno) == 1; defaultConds = [1 2 3];
else defaultConds = [1 4 5]; end

% ask for conditions
while length(intersect(cond, defaultConds)) < length(cond)
    cond = input('>>>> Enter condition numbers:  ','s');
    if length(cond) >= 1; cond = str2num(cond);
    elseif isempty(cond); cond = defaultConds;
    end
   
    if length(intersect(cond, defaultConds)) < length(cond)
        fprintf(1, 'WARNING:  At least one of the conditions you specified does not exist. \nWARNING:  You can normally only choose between conditions: %s\n', num2str(defaultConds));
        fprintf(1, 'WARNING:  However, I can overwrite the defaults for this session.\n')
        overwrite = input( 'WARNING:  Do you want me to override the session defaults [y / n]? ','s');
        if strcmp(overwrite,'y')
            break;
            owsw = 1;
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%
%   Get dominant eye   %
%%%%%%%%%%%%%%%%%%%%%%%%
defaultEye = ['L', 'R', 'B'];
dome = NaN;

% ask for dominant eye 
while length(intersect(dome, defaultEye)) < length(dome)
    dome = input('>>>> Enter dominant eye [e.g. L, R, B]:  ','s');
    if isempty(dome); dome = 'R';
    end
    if length(intersect(dome, defaultEye)) < length(dome); fprintf(1, 'WARNING:  You made a mistake when trying to specify the dominant eye. \nWARNING:  You can choose between the following options: L, R, B\n');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Combine participant ID, session number, and experiment name %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~setting.train; subjectCode = sprintf(strcat(experimentName,'_',vpno,seno));
elseif setting.train; subjectCode = sprintf(strcat(experimentName,vpno,seno,'p'));
end

end