function design = genDesign(vpno, seno, cond, dome, ms, subjectCode)
%
% Notes on the design structure:
%
% General info:
%   .nTrain - number of training trials
%   .nTrial - number of test trial
%
% Trial info (.trial is replaced by .train in practice trials):
%   .trial(t).

global visual scr keys setting %#ok<NUSED>

% strictly for testing: Emulation of propixx on low res screen: 
if setting.Pixx == 2; scr.refr = 1440; scr.fd = 1/scr.refr; end

% randomize random
rand('state',sum(100*clock));

% convenient mini functions
ceilfix = @(x)ceil(abs(x)) .* sign(x);
paren   = @(x, varargin) x(varargin{:});

% this subject's main sequence parameters
V0 = 450;A0 = 7.9;durPerDeg = 2.7;durInt = 23; 

% participant parameters
design.vpno = vpno;
design.seno = str2double(seno);
design.dome = dome;

% no. of blocks and trials per condition
if setting.train
    design.nBlocks = 1;                             % number of blocks
    design.nTrialsPerCellInBlock = 25;              % number of trials per cell in block
else
    design.nBlocks = 1;                             % number of blocks
    design.nTrialsPerCellInBlock = 2;               % number of trials per cell in block
end

% condition parameter
design.condRat = paren(repelem(cond,4), 3:length(cond) * 4);  % 1 = no stim, 2 = simulated microsaccade, 3 = active not-individualized microsaccade, 4 = microsaccade replay, 5 = active, individualized microsaccade
design.stiPres = [0 1 1 1 1];                   % 0 = no stim, 1 = stim present    
design.appShif = [0 1 0 1 0];                   % 0 = no apperture shift, 1 = apperture shift
design.sacRequ = [0 0 1 0 1];                   % 0 = no saccade, 1 = saccade
design.feBaPrs = setting.train;                 % 0 = no feedb ack, 1 = w. feedback
design.saccNec = [0 1 0 1 0];
    
% setting for fixation (before cue)
design.timFixD = 1.000 * setting.intPres;       % fixation duration before stim onset [s]

% timing settings for stimulus and cue
design.timMaSa = .0400;                         % time to make a saccade (most likely)  [s]

% onset settings for stimuli & cue
design.timStiD = 1.000;                         % stimulus stream duration [s]
design.timTarD = 0.150;                         % target presentation duration [s]
 
% other important time parameter
design.iti     = 0.000;                         % inter-trial interval [s]
design.timFeba = 0.500 * setting.train;         % time for which feedback is displayed [s]
design.timAfKe = 0.200;                         % recording time after keypress  [s]

% fixation dot parameters 
design.fixSiz = 0.1;      % size of the fixation dot (inner part) [dva]
design.gosSiz = 0.3;      % size of the fixation dot (outer part) [dva]                    

% stimulus parameters
design.numStim = 1;                                                        % number of stimuli
design.stiOri  = -pi:(2*pi/360):pi-(2*pi/360);                             % vector with possible stimulus orientations (0:359 deg)
design.eyeMoveEndpoint = [cos(design.stiOri); sin(design.stiOri)];         % vector with the X and Y coordinates of the endpoints of the apperture movements. The Y coordinate is flipped: sin() * -1! 
design.eyeMoveIdx      = reshape(repmat(randn(1,1000),1,4) * 25 + [90 180 270 360]',1,[]) ;   % % vector with indeces all of the possible apperture movements. Indceses are normally distributed around the cardnial directions: i.e. no of idx. peaks at 0, 90, 180, and 270 deg 
design.eyeMoveIdx(design.eyeMoveIdx>360) = design.eyeMoveIdx(design.eyeMoveIdx>360)-360;  % and cupped at 360 deg

% clock parameters
design.clock.nHand  = 2;        % number of clock hands to be used (either 1 or 2)
design.clock.radius = 5;        % clock radius [dva]
design.clock.handL  = 2;        % length of the clock hand [dva]
design.clock.handW  = 0.1;      % width of the clock hand [dva]
design.clock = getClockPars(design.clock.radius, design.clock.nHand, design.clock.handL, design.clock.handW);

% load in ms parameter
design.sac = getMsData(ms);

c = 0; % install counter for ALL trials
for b = 1:design.nBlocks
    t = 0;
    for cond = design.condRat 
        for itri = 1:design.nTrialsPerCellInBlock
            t = t + 1; c = c + 1;
            fprintf(1,'\n preparing trial %i ...',t);
            trial(t).trialNum = t; %#ok<*AGROW>

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %      Saccade parameter in diff. conditions      %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            switch cond
                case 1
                    % REPlay Index: In intentional microsaccade condition determined 
                    % by number of pre-defined replay szenarios.
                    repi = ceilfix(design.eyeMoveIdx(ceilfix(length(design.eyeMoveIdx) * rand))); 
                 
                    % Saccade info - since there is no stimulus at all,
                    % there is no need to define a saccade.
                    trial(t).sac.amp = 0;
                    trial(t).sac.vpk = 0;
                    trial(t).sac.dur = 0;
                    trial(t).sac.ang = NaN;

                    % Saccade is required in half of the trials.
                    trial(t).sac.req = round(rand);
                case 2
                    % REPlay Index: In intentional microsaccade condition determined 
                    % by number of pre-defined replay szenarios.
                    repi = ceilfix(design.eyeMoveIdx(ceilfix(length(design.eyeMoveIdx) * rand))); 

                    % Saccade info 
                    % Expected saccade velocities based on amplitudes
                    % (using equation and parameters used in Collewijn,
                    % 1988). Angle of saccade to be replayed is based on
                    % on the endpoints of the saccade that is replayed.
                    trial(t).sac.amp = 0.5;                                         % in dva
                    trial(t).sac.vpk = V0 * (1 - exp(-trial(t).sac.amp / A0));      % in dva/s
                    trial(t).sac.dur = durPerDeg * trial(t).sac.amp + durInt;       % should be around 25 ms according to Martinez-Conde et al. 2004 [ms]
                    trial(t).sac.ang = atan2(design.eyeMoveEndpoint(2, repi), design.eyeMoveEndpoint(1, repi)); % saccade orientation determined by predefined stimulus orientations [rad]                       

                    % ACTIVE Saccade is  required for the stimulus to be
                    % perceived 
                    trial(t).sac.req = 1;
                case 3
                    % Expected saccade velocities based on amplitudes
                    % (using equation and parameters used in Collewijn,
                    % 1988). Angle of saccade is set to 0 or pi so that the
                    % Gabor will be vertical and the phase shift directed
                    % to the right or left.
                    trial(t).sac.amp = 0.5;                                          % in dva
                    trial(t).sac.vpk = V0 * (1 - exp(-trial(t).sac.amp / A0));       % in dva/s
                    trial(t).sac.dur = durPerDeg * trial(t).sac.amp + durInt;        % in ms
                    trial(t).sac.ang = paren([0 pi], ceilfix(rand * 2));             % saccade orientation that makes the stim. visible [rad]

                    % ACTIVE saccade is NOT(!) required. Instead, MS will
                    % lead to increased stimulus visibility
                    trial(t).sac.req = 0;
                case 4
                    % REPlay Index: In microsaccade replay condition determined
                    % by number of pre-recordded saccades.
                    repi = ceilfix(size(design.sac.spatInfo,2) * rand); 

                    % Saccade info
                    % Every aspect of the replay-saccades is determined by previously recorded
                    % saccades.
                    trial(t).sac = design.sac.spatInfo(repi);                 % load spatial information of the saccade

                    trial(t).sac.amp = trial(t).sac.cVector(end) - trial(t).sac.cVector(1);          % in dva
                    trial(t).sac.vpk = max(trial(t).sac.vVector) * scr.refr;                         % in dva/s
                    trial(t).sac.dur = length(trial(t).sac.cVector) * scr.fd * 1000;                 % in ms 
                    trial(t).sac.ang = atan2(trial(t).sac.Coords(2,end),trial(t).sac.Coords(1,end)); % saccade orientation that makes the stim. visible [rad]  

                    % ACTIVE Saccade is  required for the stimulus to be
                    % perceived 
                    trial(t).sac.req = 1;
                case 5
                    % Expected saccade velocities based on amplitudes
                    % (using equation in Collewijn, 1988). The parameters for
                    % V0 and A0 are derived by fitting the saccades of the
                    % previous session/s of the participant to the equation 
                    % in Collewijn, 1988. Angle of saccade 
                    % is set to 0 or pi so that the Gabor will be upright.
                    V0 = design.sac.paraInfo.V0; A0 = design.sac.paraInfo.A0;
                    trial(t).sac.amp = design.sac.paraInfo.sacAmp;                 % saccade amplitude is chosen so that 75% of all previous saccades were larger [dva]
                    trial(t).sac.vpk = V0 * (1 - exp(-trial(t).sac.amp / A0));     % in dva/s
                    trial(t).sac.dur = durPerDeg * trial(t).sac.amp + durInt;      % in ms 
                    trial(t).sac.ang = paren([0 pi], ceilfix(rand * 2));           % saccade orientation that makes the stim. visible [rad]

                    % ACTIVE saccade is NOT(!) required. Instead, MS will
                    % lead to increased stimulus visibility
                    trial(t).sac.req = 0;
            end

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %             Spatial trial settings              %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % fixation positions
            fixx = -13 + (13 + 13) * rand;   % eccentricity of fixation x (relative to screen center) - can be pos or neg [dva]
            fixy =  -4 + ( 4 +  4) * rand;   % eccentricity of fixation y (relative to screen center) - can be pos or neg [dva]

            % target
            switch cond
                case 1
                    if design.seno == 1
                        saepX = design.eyeMoveEndpoint(1, repi);
                        saepY = design.eyeMoveEndpoint(2, repi);
                        taxx = fixx + saepX - (saepX + saepX) * rand;
                        taxy = fixy + saepY - (saepY + saepY) * rand;
                    else
                        taxx = fixx + trial(t).sac.Coords(1,end);
                        taxy = fixy + trial(t).sac.Coords(2,end) * -1;
                    end
                case 2
                    saepX = design.eyeMoveEndpoint(1, repi);
                    saepY = design.eyeMoveEndpoint(2, repi);
                    taxx = fixx + saepX - (saepX + saepX) * rand;
                    taxy = fixy + saepY - (saepY + saepY) * rand;
                case 4
                    taxx = fixx + trial(t).sac.Coords(1,end);
                    taxy = fixy + trial(t).sac.Coords(2,end) * -1;
                otherwise
                    taxx = NaN;
                    taxy = NaN;
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %            Temporal trial settings              %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % duration before stimulus onset [frames]
            trial(t).fixNFr = round((design.timFixD) / scr.fd);
            trial(t).fixDur = trial(t).fixNFr * scr.fd;

            % duration befor/after potential target on-/offset [frames]
            trial(t).traNFr = round((design.timStiD / 5) / scr.fd);
            trial(t).traDur = trial(t).traNFr * scr.fd;

            % duration after target onset [frames]
            trial(t).tarNFr = round(design.timTarD / scr.fd);
            trial(t).tarDur = trial(t).tarNFr * scr.fd;

            % duration after stimulus onset [frames]
            trial(t).stiNFr = round((design.timStiD) / scr.fd);
            trial(t).stiDur = trial(t).stiNFr * scr.fd;
            
            % duration after eye movement [frames]
            trial(t).eyeNFr = round(design.timMaSa / scr.fd);
            trial(t).eyeDur = trial(t).eyeNFr * scr.fd;
            
            % duration of stimulus contrast ram [frames]
            trial(t).ramNFr = round((design.timStiD / 5) / scr.fd);
            trial(t).ramDur = trial(t).ramNFr * scr.fd;

            % timing parameters of apperture shift [frames]
            % (saccade duration is a good approximation of the duration of
            % the apperture shift)
            trial(t).shiNFr = round((trial(t).sac.dur / 1000) / scr.fd);       
            trial(t).shiDur = trial(t).shiNFr * scr.fd;

            % duration after feedback onset [frames]
            trial(t).febNFr = round(design.timFeba / scr.fd);
            trial(t).febDur = trial(t).febNFr * scr.fd;

            % calculate total stimulus duration [frames]
            trial(t).totNFr = trial(t).fixNFr + trial(t).stiNFr;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Generate flag streams for stimulus presentation %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % time of the fixation            
            fixBeg = 1;
            fixEnd = setting.intPres * trial(t).fixNFr + ~setting.intPres * trial(t).totNFr;      

            % begin and end of the time window in which the target MAY
            % appear
            traBeg = fixBeg + trial(t).traNFr;
            traEnd = fixEnd - trial(t).traNFr - trial(t).tarNFr;

            % begin and end of the time window in which the target IS
            % present
            tarBeg = traBeg + round((traEnd - traBeg) * rand); 
            tarEnd = tarBeg + trial(t).tarNFr;

            % time that the stimulus is present
            stiBeg = trial(t).fixNFr + 1;
            stiEnd = stiBeg + trial(t).stiNFr - 1;

            % time that the stimulus is at full contrast
            ctrBeg = stiBeg + trial(t).ramNFr;                              
            ctrEnd = stiEnd - trial(t).ramNFr; 

            % begin and end of the time window in which the apperture MAY move
            shiBeg = ctrBeg + round(trial(t).shiNFr / 2);
            shiEnd = ctrEnd - round(trial(t).shiNFr / 2);

            % time parameter of apperture movement
            switch cond
                case 2
                    % begin and end of the time window in which the target MAY
                    % appear
                    traBeg = fixBeg + trial(t).traNFr;
                    traEnd = fixEnd - trial(t).traNFr - trial(t).tarNFr;

                    % begin and end of the time window in which the target IS
                    % present
                    tarBeg = traBeg + round((traEnd - traBeg) * rand); 
                    tarEnd = tarBeg + trial(t).tarNFr;
                          
                    % time point at which the eye movement cue is give and
                    % (fix dot disappears) and the feedback is given
                    % (reappearance at target location)
                    cueBeg = stiBeg + round(trial(t).stiNFr/5 * rand);
                    cueEnd = cueBeg + trial(t).eyeNFr;
                case 4
                    % begin and end of the time window in which the target MAY
                    % appear
                    traBeg = fixBeg + trial(t).traNFr;
                    traEnd = fixEnd - trial(t).traNFr - trial(t).tarNFr;

                    % begin and end of the time window in which the target IS
                    % present
                    tarBeg = traBeg + round((traEnd - traBeg) * rand); 
                    tarEnd = tarBeg + trial(t).tarNFr;
                otherwise
                    traBeg = NaN;    
                    traEnd = NaN;    
                    tarBeg = NaN;
                    tarEnd = NaN;
            end

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %              spatial information                %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % fixation
            trial(t).fixa.vis = zeros(1,trial(t).totNFr);
            if trial(t).fixa.vis(fixBeg:stiEnd) = 1;
            trial(t).fixa.loc = visual.scrCenter + round(visual.ppd * [fixx fixy fixx fixy]);
            trial(t).fixa.sin = round(design.fixSiz * visual.ppd);
            trial(t).fixa.sou = round(design.gosSiz * visual.ppd);
            trial(t).fixa.col = visual.black;

            % target
            trial(t).targ.vis = zeros(1,trial(t).totNFr);
            if ~isnan(tarBeg); trial(t).targ.vis(tarBeg:tarEnd) = design.saccNec(cond); end
            trial(t).targ.loc = visual.scrCenter + round(visual.ppd * [taxx taxy taxx taxy]);
            trial(t).targ.sin = round(design.fixSiz * visual.ppd);
            trial(t).targ.col = visual.white;

            % determine stimulus parameters
            % spatial stimulus settings (standard)
            trial(t).stims.locX = visual.scrCenter(1) + round(visual.ppd * fixx); 
            trial(t).stims.locY = visual.scrCenter(2) + round(visual.ppd * fixy);

            % determine stimulus parameters
            % spatial stimulus settings (standard)
            trial(t).stims.col  = repmat(visual.black,design.numStim,1);
            trial(t).stims.pars = getGaborPars(design.numStim);
            trial(t).stims.pars.ori = paren([0 180], round(1+rand));

            % define change in stimulus position across frames
            trial(t).posVec = zeros(design.numStim * 2,trial(t).totNFr); 

            % define modulation of top velocity across frames
            % (here, we are keeping stimulus velocity constant)
            velVec = ones(1,length(stiBeg:stiEnd));
            trial(t).stims.tmpfrq = 0; 

            % changed from saccade dir. to movement 
            % dir because one can no longer infer the
            % saccade direction from  stimulus properties 
            % (as has been done originally)
            trial(t).stims.evovel = repmat(velVec*trial(t).sac.vpk,design.numStim,1) + repmat(trial(t).stims.tmpfrq ./ trial(t).stims.pars.frq',1,length(velVec)); % desired stimulus velocity [dva per sec]

            % define phase change per frame for entire profile
            phaFra = scr.fd*trial(t).stims.evovel.*repmat(trial(t).stims.pars.frq'*360,1,length(velVec));  % phase change per frame [deg per fra]

            % define stimulus visibility and velocity
            trial(t).stims.vis = zeros(design.numStim,trial(t).totNFr);
            trial(t).stims.pha = zeros(design.numStim,trial(t).totNFr);
            trial(t).stims.vis(:,stiBeg:stiEnd) = trial(t).sac.req;
            trial(t).stims.pha(:,stiBeg:stiEnd) = cumsum(phaFra,2);

            % add random phase to each stimulus
            trial(t).stims.pha = trial(t).stims.pha + repmat(360*rand(design.numStim,1),1,trial(t).totNFr);

            % define modulation of contrast across time
            ampVec = ones(1,length(stiBeg:stiEnd));
            ramVec = normcdf(1: trial(t).ramNFr, trial(t).ramNFr/2, trial(t).ramNFr/6);
            ampVec(1: trial(t).ramNFr) = ramVec;
            ampVec(end:-1:(end- trial(t).ramNFr+1)) = ramVec;
            trial(t).stims.evoamp = repmat(trial(t).stims.pars.amp',1,length(ampVec)) .* repmat(ampVec,design.numStim,1);
            trial(t).stims.amp = zeros(design.numStim,trial(t).totNFr);
            trial(t).stims.amp(:,stiBeg:stiEnd) = trial(t).stims.evoamp;

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %         response & feedback information         %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % define clock parameter
            trial(t).clock = design.clock;   
            cRand = ceilfix(trial(t).clock.Speed / trial(t).clock.nHand * rand);
            cRandR = cRand;
            respOpt = trial(t).clock.Speed;
            trial(t).clock.facePos = design.clock.facePos + trial(t).fixa.loc(1:2)';
            trial(t).clock.handPos = design.clock.handPos + trial(t).fixa.loc';
            if rand < 0.5, trial(t).clock.faceCol = fliplr(design.clock.faceCol); end 

            % determine stimulus visibility
            trial(t).clock.vis = zeros(1,trial(t).totNFr);
            trial(t).clock.vis(stiBeg:stiEnd) = 1; 

            % Block name messages (per the 4 different conditions)
            if setting.train, trial(t).message = sprintf('This is a training block (with feedback)');
            else trial(t).message = sprintf(''); end

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % define critical events during stimulus presentation %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            trial(t).events = zeros(1,trial(t).totNFr);
            trial(t).events(fixBeg) = 1;
            trial(t).events(fixEnd) = 2;
            trial(t).events(stiBeg) = 3;
            trial(t).events(ctrBeg) = 4;
%             switch cond
%                 case {2, 4}
%                     trial(t).events(appBeg) = 5;
%                     trial(t).events(appTop) = 6;
%             end
            trial(t).events(ctrEnd) = 7;
            trial(t).events(stiEnd) = 8;

            % time requirements for responses
            trial(t).maxSac = design.timMaSa;
            trial(t).aftKey = design.timAfKe;

            % store trial features
            trial(t).feedback = setting.train;              % 1 = feedback, 0 = no feedback
            trial(t).expcon = cond;                         % 1 = no stimulus, simulated microsaccade condition, active microsacccade condition

            % store stimulus features
            trial(t).fixpox = fixx;                         % x coordinate of position of fixation [dva rel. to midpoint]
            trial(t).fixpoy = fixy;                         % y coordinate of position of fixation [dva rel. to midpoint] 
            trial(t).appbeg = 1;%appBeg * scr.fd * 1000;       % time point at which apperture shift starts [ms]
            trial(t).apptop = 1;%appTop * scr.fd * 1000;       % time point at which apperture shift reaches max velocity [ms]
            trial(t).appdur = 1;%appdur * scr.fd * 1000;       % duration of the displayed microsaccade [ms]
            trial(t).appmdX = 1;% appmdX;                       % x coordinate of position shift of the apperture [dva]
            % BC the screen coordinates system differes from the one used
            % for the calculations, these are the coordinates and angles as
            % displayed on screen...
            trial(t).appmdY_scr = 1; % appmdY;                   % y coordinate of position shift of the apperture IN SCREEN COORDINATES [dva]
            trial(t).appori_scr = 1; % round(rad2deg(atan2(trial(t).appmdY_scr,trial(t).appmdX)));  % appeture shift orientation IN SCREEN COORDINATES [deg]   
            trial(t).stiori_scr = 1; % trial(t).stims.pars.ori;  % orientation of the stimulus in space IN SCREEN COORDINATES [deg]
            % ...and these are the coordinates and angles in the original
            % calculations (i.e. based on the saccadic coordinate system).
            trial(t).appmdY_sac = 1; % appmdY * -1;              % y coordinate of position shift of the apperture FLIPPED BACK to saccade coordinates [dva]
            trial(t).appori_sac = 1; % round(rad2deg(atan2( trial(t).appmdY_sac,  trial(t).appmdX))); % appeture shift orientation FLIPPED BACK to saccade coordinates [deg]   
            trial(t).stiori_sac = 1; % 180 - trial(t).stims.pars.ori;
            trial(t).appamp = 1; % sqrt(appmdX^2 + appmdY^2);    % amplitude of the position shift of the apperture (here the difference in coordinate systems does not matter) [dva]

            % store some clock features
            trial(t).cSpeed =     scr.refr * 2;                               % clock speed as points on track
            trial(t).cSpeedpDgr = (trial(t).cSpeed / 360 * (scr.fd*1000));    % clock speed as duration per degree [ms]
            trial(t).respOpt =    respOpt;                                    % number of potential responses 
            trial(t).onPos =      cRand;                                      % onset position of clock hand [frame idx]
            trial(t).onPosR =     cRandR;                                     % onset position of clock hand [frame idx]
            trial(t).ofPos =      trial(t).onPos + trial(t).stiNFr;           % offset position of clock hand [frame idx]
            trial(t).fbPos =      1; %  cRand + appBeg - trial(t).fixNFr;           % position of clockhand at thev onset of the apperture shift [frame idx]

            %%%%%%%%%%%%%%%%%%%%%%%
            % draw loading screen %
            %%%%%%%%%%%%%%%%%%%%%%%
            arc = linspace(1, 360, design.nBlocks * design.nTrialsPerCellInBlock * length(design.condRat));
            drawLoadingScreen(visual.black, visual.scrCenter, 5, arc(c), scr.main);
        end
        r = randperm(t);                        % randomise the trials per block
        if ~setting.train
            design.b(:,b).train = [];
            design.b(:,b).trial = trial(r);     % randomise in which order the condtions are presented (in experiment)
        elseif setting.train
            design.b(:,b).train = trial(r);     % randomise in which order the condtions are presented (in training)
            design.b(:,b).trial = [];
        end
    end
end

design.nBlocks = b;                             % this number of blocks is shared by all qu
design.blockOrder = randperm(b);                % org: design.blockOrder = 1:b;

design.nTrialsPB = t;                           % number of trials per Block

% strictly for testing: Emulation of propixx on low res screen: 
if setting.Pixx == 2; scr.refr = 60; scr.fd = 1/scr.refr; setting.Pixx = 0; end
 
save(sprintf('%s.mat',subjectCode),'design','visual','scr','keys','setting');
