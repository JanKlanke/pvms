function sac = getMsData(ms)
%
% read in microsaccade velocity profiles from files
% input: vp number
% output: array with data velocity profiles, number of saccade, duration,
% peak velocity, distance, and angle
%
% 2020 by Jan Klanke

% return to main script if ms data is not available (i.e. in 1st session)
sac.spatInfo=[];sac.tempInfo=[];sac.paraInfo=[];

if ~any(structfun(@isempty, ms))
    
% load coordinates and velocities for the replay
    [~, ~, idx] = unique(ms.coords.data(:,1:4), 'rows');
    for i = 1:max(idx)
        
        % get raw coordinates and velocities (x and y)
        sac.spatInfo(i).Coords = ms.coords.data(idx==i,7:8)';        % load in the x coordinates of the MS [dva]
        sac.spatInfo(i).Velos  = ms.coords.data(idx==i,10:11)';

        % calculte 1D position vector and the velocity along it
        sac.spatInfo(i).cVector = sqrt(sac.spatInfo(i).Coords(1,:).^2 + sac.spatInfo(i).Coords(2,:).^2);
        sac.spatInfo(i).vVector = diff(sac.spatInfo(i).cVector);
    end

    % load onsets of the MS in ms and frames
    sac.tempInfo.MS  = ms.onsets.data(:,4)';
    sac.tempInfo.NFr = ms.onsets.data(:,5)';

    % load parameters of fitted main sequence
    sac.paraInfo.V0 =  ms.params.data(2);
    sac.paraInfo.A0 =  ms.params.data(3);
    sac.paraInfo.sacAmp =  ms.params.data(4);

end

end