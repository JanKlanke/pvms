
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Variance-evaluation of paradigms to measure temporal events %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 2019 by Jan Klanke
%
% 
% DESKRIPTION
%
%
%
% TODOs
%
% 
%

clear all;
clear mex;
clear functions; 
delete(instrfindall);

addpath('Functions/','Data/','Data/replayCoords/');

home;
expStart=tic;

global setting visual scr keys re %#ok<*NUSED>

setting.TEST = 2;         % test in dummy mode? 0=with eyelink; 1=mouse as eye; 2=no gaze position checking of any sort
setting.sloMo= 1;         % To play stimulus in slow motion, draw every frame setting.sloMo times
setting.train= 0;         % do you want to run a pracitce block?
setting.Pixx = 1;         % Are you using the propixx or not
setting.intPres = 1;

if setting.Pixx; re = RotaryEncoder('/dev/ttyS101'); end   % get rotary encoder (if you are inlabin the proper settup)
   
exptname='PVMS';

try 
    newFile = 0;
    while ~newFile
        [vpno, seno, cond, coow, dome, subjectCode] = prepExp(exptname);
        subjectCode = strcat('Data/',subjectCode);
        
        % create data file
        datFile = sprintf('%s.dat',subjectCode);
        if exist(datFile,'file')
            o = input('>>>> This file exists already. Should I overwrite it [y / n]? ','s');
            if strcmp(o,'y')
                newFile = 1;
            end
        else
            newFile = 1;
        end
    end
    
    % prepare replay condition
    start = 1;
    while start
        [ms, start, abort] = prepReplayCondition(vpno, seno, coow, start);
        if abort; return; end
    end
    
    % prepare screens
    prepScreen;
    
    % get key assignment
    getKeyAssignment;
    
    % disable keyboard
    ListenChar(2);
          
    % prepare stimuli
    prepStim;
    
    % generate design

    design = genDesign(vpno, seno, cond, dome, ms, subjectCode);
    
    % initialize eyelink-connection
    if setting.TEST<2
        [el, err]=initEyelinkNew(subjectCode(6:end));
        if err==el.TERMINATE_KEY
            return
        end
    else
        el=[];
    end
    
    % runtrials
    design = runTrials(design,datFile,el);
    
    % shut down everything
    reddUp;
    
catch me
    rethrow(me); 
    reddUp; %#ok<UNRCH>
end

expDur=toc(expStart);

fprintf(1,'\n\nThis (part of the) experiment took %.0f min.',(expDur)/60);
fprintf(1,'\n\nOK!\n');

% plotReplayResults(datFile);